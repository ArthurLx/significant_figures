//////////////////////////Чередующееся с goto

#include <iostream> 
#include <iomanip> 
using namespace std; 

int main() 
{ 
    const int n = 100; 
    float array[n], tmp; 
    
    bool say = true;
    for(int i = 0; i < n; i++) 
    { 
        array[i] = rand()%100; 
        array[i] = (array[i] / 10)*5; 
        
        cout << array[i] << setw(5); 
        
        tmp = array[i] - (int)array[i]; 
        
        if (say == true){   
            if (tmp == 0.5) array[i] = array[i] + (1 - tmp); 
            say = false;
            goto L1;
        }
        
        if (say == false){   
            if (tmp == 0.5) array[i] = array[i] - tmp; 
            say = true;
            goto L1;
        }
        
        if (tmp > 0.5) array[i] = array[i] + (1 - tmp);
        else if (tmp < 0.5) array[i] = array[i] - tmp; 

        L1:
        cout << array[i] << endl;
    }
    
return 0; 
}


////////////////////////////////Чередующееся без goto

#include <iostream> 
#include <iomanip> 
using namespace std; 

int main() 
{ 
    const int n = 100; 
    float array[n], tmp; 
    
    bool say = true;
    for(int i = 0, j = 1; i < n; i++, j++) //j это флаг чередования
    { 
        array[i] = rand()%100; 
        array[i] = (array[i] / 10)*5; 
        
        cout << array[i] << setw(5); 
        
        tmp = array[i] - (int)array[i]; 
        
        if (j % 2 != 0){                   //если попадаем на нечетный раз то в большую
            if (tmp == 0.5) array[i] = array[i] + (1 - tmp); 
        }
        
        else if (j % 2 == 0){              //если попадаем на четный раз то в меньшую
            if (tmp == 0.5) array[i] = array[i] - tmp; 
        }
        
        if (tmp > 0.5) array[i] = array[i] + (1 - tmp);
        else if (tmp < 0.5)  array[i] = array[i] - tmp; 

        cout << array[i] << endl;
    }
    
return 0; 
} 