/////////////////////////Ближайшее четное

#include <iostream> 
#include <iomanip> 
using namespace std; 

int main() 
{ 
    const int n = 100; 
    float array[n], tmp; 

    for(int i = 0; i < n; i++) 
    { 
        array[i] = rand()%100; 
        array[i] = (array[i] / 10)*3; 
        
        cout << array[i] << setw(5); 
        
        tmp = array[i] - (int)array[i]; 
        if (tmp == 0.5 && (int)array[i] % 2 != 0) array[i] = array[i] + (1 - tmp); //Целая часть числа нечетная, след округление в большую сторону
        else if (tmp == 0.5 && (int)array[i] % 2 == 0) array[i] = array[i] - tmp; //Целая часть числа четная, след округление в меньшую сторону
    
        else{                   //Стандартное округление
            if (tmp > 0.5) array[i] = array[i] + (1 - tmp);
            else if (tmp < 0.5) array[i] = array[i] - tmp;
        }

        cout << array[i] << endl;
    }
    
return 0; 
}