///////////////////////////Случайное округление

#include <iostream> 
#include <iomanip> 
using namespace std; 

int main() 
{ 
    
    const int n = 100; 
    float array[n], tmp; 
    
    bool say = true;
    for(int i = 0, j = 1; i < n; i++, j++) 
    { 
        array[i] = rand()%100; 
        array[i] = (array[i] / 10)*5; 
        
        cout << array[i] << setw(5); 
        
        tmp = array[i] - (int)array[i]; 
        
        if ((rand()%100)%2 != 0){
            if (tmp == 0.5) array[i] = array[i] + (1 - tmp); 
        }
        else{
            if (tmp == 0.5) array[i] = array[i] - tmp; 
        }
        
        if (tmp > 0.5) array[i] = array[i] + (1 - tmp);
        else if (tmp < 0.5)  array[i] = array[i] - tmp; 

        cout << array[i] << endl;
    }
    
return 0; 
}