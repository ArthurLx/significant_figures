//////////////////Стандартное округление

#include <iostream> 
#include <iomanip> 
using namespace std; 

int main() 
{ 
    const int n = 100; 
    float array[n], tmp; 

    for(int i = 0; i < n; i++) 
    { 
        array[i] = rand()%100; 
        array[i] = (array[i] / 10)*3; 
        
        cout << array[i] << setw(5); 
        
        tmp = array[i] - (int)array[i];      // Получение десятичной части
        if (tmp >= 0.5) array[i] = array[i] + (1 - tmp); //Условное округление
        else array[i] = array[i] - tmp; 
        
        cout << array[i] << endl;
    }
    
return 0; 
} 